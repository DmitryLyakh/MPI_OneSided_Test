/*
 * Copyright (c) 2016       UT-Battelle, LLC
 *                          All Rights Reserved.
 * AUTHOR(s): Geoffroy Vallee
 *            Dmitry I. Lyakh
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mpi.h"

/* Buffer/message volumes */
#define MAX_BUFFER_VOL (512*1024*1024)
#define MIN_MSG_VOL (128*1024*1024)
#define MSG_INCR_VOL (128*1024*1024)

/* Number of loop interations over MPI_Get() and MPI_Win_flush()
   with differnt compute time */
#define N_LOOPS (4)


void comput(double ctime)
{
 double tms,tm;
 tms = MPI_Wtime();
 tm = 0.0;
 while(tm < ctime)
 {
  tm = MPI_Wtime() - tms;
 };
 return;
}

int
main (int argc, char **argv)
{
    int         rc;
    MPI_Win     my_win;
    int         *snd_buf    = NULL;
    int         *recv_buf   = NULL;
    int         world_size;
    int         my_rank;
    int         target_rank;
    int         origin_rank;
    int         msg_vol;
    double      comp_time;
    double      max_get_time;
    double      get_time;
    double      max_flush_time;
    double      flush_time;
    double      start_time;
    double      end_time;
    double      overlap;
    double      bandwidth;
    int         i;
    MPI_Aint    local_displ;
    MPI_Aint    target_displ;
    MPI_Request send_req;
    MPI_Status  status;

    rc = MPI_Init (&argc, &argv);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Init() failed\n");
        return EXIT_FAILURE;
    }

    rc = MPI_Comm_size (MPI_COMM_WORLD, &world_size);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Comm_size() failed\n");
        goto exit_on_failure;
    }

    rc = MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Comm_rank() failed\n");
        goto exit_on_failure;
    }

    /* Define the local target for the MPI_Get() operation */
    if (my_rank == 0) {
        target_rank = world_size - 1;
    } else {
        target_rank = my_rank - 1;
    }

    /* Define the remote origin that will issue a MPI_Get() */
    if (my_rank  == world_size - 1) {
        origin_rank = 0;
    } else {
        origin_rank = my_rank + 1;
    }

    /* Create the dynamic window, assuming we do not have the buffers just
       yet */
    rc = MPI_Win_create_dynamic (MPI_INFO_NULL,
                                 MPI_COMM_WORLD,
                                 &my_win);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Win_create_dynamic() failed\n");
        goto exit_on_failure;
    }

    /* We create a buffer so we can try to overlap computation and
       one-sided communications. */
    rc = posix_memalign ((void**)&snd_buf, 4096, MAX_BUFFER_VOL * sizeof (int));
    if (rc < 0) {
        fprintf (stderr, "ERROR: Memory allocation failed\n");
        goto exit_on_failure;
    }

    rc = posix_memalign ((void**)&recv_buf, 4096, MAX_BUFFER_VOL * sizeof (int));
    if (recv_buf == NULL) {
        fprintf (stderr, "ERROR: Memory allocation failed\n");
        goto exit_on_failure;
    }

    rc = MPI_Barrier (MPI_COMM_WORLD);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Barrier() failed\n");
        goto exit_on_failure;
    }

    /* Declare the memory as remotely accessible. */
    rc = MPI_Win_attach (my_win, snd_buf, MAX_BUFFER_VOL * sizeof (int));
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Win_attach() failed\n");
        goto exit_on_failure;
    }

    /* Get the displacement and send it to the origin rank */
    rc = MPI_Get_address (&snd_buf[0], &local_displ);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Get_address() failed\n");
        goto exit_on_failure;
    }

    fprintf (stdout,
             "Rank %d: Sending addr (%p) to rank %d\n",
             my_rank,
             (void*)local_displ,
             origin_rank);
    rc = MPI_Isend ((void*)&local_displ,
                   1,
                   MPI_AINT,
                   origin_rank,
                   0,
                   MPI_COMM_WORLD,
                   &send_req);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Send() failed\n");
        goto exit_on_failure;
    }

    /* Receive the displacement from the target rank */
    fprintf (stdout,
             "Rank %d: Recving addr from rank %d\n",
             my_rank,
             target_rank);
    rc = MPI_Recv ((void*)&target_displ,
                   1,
                   MPI_AINT,
                   target_rank,
                   0,
                   MPI_COMM_WORLD,
                   &status);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Recv() failed\n");
        goto exit_on_failure;
    }

    rc = MPI_Wait (&send_req, &status);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Wait() failed\n");
        goto exit_on_failure;
    }

    fprintf (stdout,
             "Rank %d: Target displacement (rank %d) is %p\n",
             my_rank,
             target_rank,
             (void*)target_displ);

    rc = MPI_Barrier (MPI_COMM_WORLD);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Barrier() failed\n");
        goto exit_on_failure;
    }

    /* Everything is ready, we loop over MPI_Get() calls and some fake
       processing to simulate the overlapping of computation and
       communications */
    rc = MPI_Win_lock_all (0, my_win);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "ERROR: MPI_Win_lock_all() failed\n");
        goto exit_on_failure;
    }

    msg_vol = MIN_MSG_VOL;
    while(msg_vol <= MAX_BUFFER_VOL)
    {
        fprintf (stdout, "[%d]: Message size (bytes) = %lu \n", my_rank,msg_vol*sizeof(int));

        comp_time = 0.0;
        for (i = 0; i <= N_LOOPS; i++)
        {
            start_time = MPI_Wtime();
            rc = MPI_Get (&recv_buf[0],
                          msg_vol,
                          MPI_INT,
                          target_rank,
                          target_displ,
                          msg_vol,
                          MPI_INT,
                          my_win);
            if (rc != MPI_SUCCESS) {
                fprintf (stderr, "ERROR: MPI_Get() failed\n");
                goto exit_on_failure;
            }
            end_time = MPI_Wtime();
            get_time = end_time - start_time;
            fprintf (stdout, "[%d]: Get took %f seconds\n", my_rank,get_time);
            if(i == 0) max_get_time = get_time;

            comput(comp_time);
            fprintf (stdout, "[%d]: Computation took %f seconds\n", my_rank,comp_time);

            start_time = MPI_Wtime();
            rc = MPI_Win_flush (target_rank, my_win);
            if (rc != MPI_SUCCESS) {
                fprintf (stderr, "ERROR: MPI_Win_flush() failed\n");
                goto exit_on_failure;
            }
            end_time = MPI_Wtime();
            flush_time = end_time - start_time;
            fprintf (stdout, "[%d]: Flush took %f seconds\n", my_rank,flush_time);
            if(i == 0) max_flush_time = flush_time;

            if(i == 0){
             bandwidth = (double)(msg_vol*sizeof(int)) / (max_get_time + max_flush_time);
             fprintf (stdout, "[%d]: Bandwidth (GB/s) = %f\n", my_rank,bandwidth/1024./1024./1024.);
            }else{
             overlap = ((max_get_time + max_flush_time) - (get_time + flush_time)) / comp_time;
             fprintf (stdout, "[%d]: Overlap (0..1) = %f\n", my_rank,overlap);
            }
            comp_time += (max_get_time + max_flush_time) / (double)N_LOOPS;
        }
        rc = MPI_Barrier (MPI_COMM_WORLD);
        if (rc != MPI_SUCCESS) {
            fprintf (stderr, "ERROR: MPI_Barrier() failed\n");
            goto exit_on_failure;
        }
        msg_vol += MSG_INCR_VOL;
    }

    rc = MPI_Win_unlock_all (my_win);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "MPI_Win_unlock_all() failed\n");
        goto exit_on_failure;
    }

    /* All done, finalizing... */
    rc = MPI_Win_detach (my_win, snd_buf);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "MPI_Win_detach() failed\n");
    }

    rc = MPI_Win_free (&my_win);
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "MPI_Win_free() failed\n");
    }

    if (snd_buf != NULL) {
        free (snd_buf);
        snd_buf = NULL;
    }

    if (recv_buf != NULL) {
        free (recv_buf);
        recv_buf = NULL;
    }

    rc = MPI_Finalize ();
    if (rc != MPI_SUCCESS) {
        fprintf (stderr, "MPI_Finalize() failed\n");
        return EXIT_SUCCESS;
    }

    return EXIT_SUCCESS;

 exit_on_failure:
    if (snd_buf != NULL) {
        free (snd_buf);
        snd_buf = NULL;
    }

    if (recv_buf != NULL) {
        free (recv_buf);
        recv_buf = NULL;
    }

    MPI_Finalize ();

    return EXIT_FAILURE;
}
